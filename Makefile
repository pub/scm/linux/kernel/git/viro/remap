.c.o:
        cc -std=c99 -c -g -Wall -pedantic -o $@ $<

remap-log: remap-log.o
	cc -g -o $@ $^

clean:
	-rm -f *.o remap-log
